import org.apache.spark.sql.types.{DoubleType, IntegerType, StringType, StructField, StructType}
import org.apache.spark.sql.SparkSession

object HomeworkStreaming {
  def main(args: Array[String]): Unit = {
    val files = Array(
      "D:\\BigData\\weather\\year=2016",
      "D:\\BigData\\weather\\year=2017",
      "D:\\BigData\\expediaJSON",
      "D:\\BigData\\hotels"
    )
    val spark = SparkSession
      .builder()
      .master("local[*]")
      .appName("SparkStreaming")
      .getOrCreate

    import spark.implicits._

    spark.sparkContext.setLogLevel("ERROR")

    val schemaForHotels = (new StructType).add("channel", (new StructType()).add("int", StringType))
      .add("date_time", (new StructType()).add("string", StringType))
      .add("hotel_id", (new StructType()).add("long", StringType))
      .add("id", (new StructType()).add("long", StringType))
      .add("is_mobile", (new StructType()).add("int", StringType))
      .add("is_package", (new StructType()).add("int", StringType))
      .add("orig_destination_distance", (new StructType()).add("double", StringType))
      .add("posa_continent", (new StructType()).add("int", StringType))
      .add("site_name", (new StructType()).add("int", StringType))
      .add("srch_adults_cnt", (new StructType()).add("int", StringType))
      .add("srch_children_cnt", (new StructType()).add("int", IntegerType))
      .add("srch_ci", (new StructType()).add("string", StringType))
      .add("srch_co", (new StructType()).add("string", StringType))
      .add("srch_destination_id", (new StructType()).add("int", StringType))
      .add("srch_destination_type_id", (new StructType()).add("int", StringType))
      .add("srch_rm_cnt", (new StructType()).add("int", StringType))
      .add("user_id", (new StructType()).add("int", StringType))

    val schemaForHotelNames = StructType(
      List(
        StructField("HotelId", StringType, true),
        StructField("HotelName", StringType, true),
        StructField("Country", StringType, true),
        StructField("City", StringType, true),
        StructField("Address", StringType, true),
        StructField("Latitude", StringType, true),
        StructField("Longitude", StringType, true)
      )
    )

    val schemaForWeather = StructType(
      List(
        StructField("lng", DoubleType, true),
        StructField("lat", DoubleType, true),
        StructField("avg_tmpr_f", DoubleType, true),
        StructField("avg_tmpr_c", DoubleType, true),
        StructField("wthr_date", StringType, true),
        StructField("month", IntegerType, true),
        StructField("day", IntegerType, true)
      )
    )

    val weatherFor2016 = spark.readStream.schema(schemaForWeather).parquet(files(0))
    weatherFor2016.createOrReplaceTempView("weatherFor2016")
    val weatherFor2017 = spark.readStream.schema(schemaForWeather).parquet(files(1))
    weatherFor2017.createOrReplaceTempView("weatherFor2017")
    val hotels = spark.readStream.schema(schemaForHotels).json(files(2))
    hotels.createOrReplaceTempView("hotels")
    val hotelNames = spark.readStream.schema(schemaForHotelNames).csv(files(3))
    hotelNames.createOrReplaceTempView("hotelNames")

    // join 2016 and 2017 years
    val weather = spark.sql("SELECT * FROM weatherFor2016 UNION ALL SELECT * FROM weatherFor2017")
    weather.createOrReplaceTempView("weather")

    // add average day temperature
    // filter incoming data by having average temperature more than 0 Celsius degrees
    val averageTemperature = spark.sql(
      "SELECT wthr_date, AVG(avg_tmpr_f) avg_f, AVG(avg_tmpr_c) avg_c " +
        "FROM weather GROUP BY wthr_date HAVING avg_c>0")
    averageTemperature.createOrReplaceTempView("averageTemperature")

    // join with hotels + weather data
    val temperatureAtCheckIn = spark.sql("SELECT DISTINCT c.srch_ci, o.avg_c FROM hotels c " +
      "INNER JOIN averageTemperature o ON (c.srch_ci.string = o.wthr_date)")
    temperatureAtCheckIn.createOrReplaceTempView("temperatureAtCheckIn")

    // duration of stay as days between requested check-in and check-out date
    // each hotel with multi-dimensional state consisting of record counts for each type of stay
    // children presence in booking
    val durationOfStay = spark.sql("SELECT hotel_id, " +
      "DATEDIFF(CAST(srch_co.string AS date), CAST(srch_ci.string AS date)) AS DurationOfStay, " +
      "CASE WHEN DATEDIFF(CAST(srch_co.string AS date), CAST(srch_ci.string AS date)) >=1 " +
      "AND DATEDIFF(CAST(srch_co.string AS date), CAST(srch_ci.string AS date))<=3 THEN 'Short stay' " +
      "WHEN DATEDIFF(CAST(srch_co.string AS date), CAST(srch_ci.string AS date)) >=4 " +
      "AND DATEDIFF(CAST(srch_co.string AS date), CAST(srch_ci.string AS date))<=7 THEN 'Standart stay' " +
      "WHEN DATEDIFF(CAST(srch_co.string AS date), CAST(srch_ci.string AS date)) >=8 " +
      "AND DATEDIFF(CAST(srch_co.string AS date), CAST(srch_ci.string AS date))<=14 THEN 'Standart extended stay' " +
      "WHEN DATEDIFF(CAST(srch_co.string AS date), CAST(srch_ci.string AS date)) >=15 " +
      "AND DATEDIFF(CAST(srch_co.string AS date), CAST(srch_ci.string AS date))<=28 THEN 'Long stay' " +
      "ELSE 'Erroneous data' END AS StateOfStay, " +
      "CASE WHEN srch_children_cnt.int =0 THEN 'NO' " +
      "WHEN srch_children_cnt.int >0 THEN 'YES' END AS ChildrenPresence FROM hotels")
    durationOfStay.createOrReplaceTempView("durationOfStay")

    // add hotel names
    val resultTable = spark.sql("SELECT c.HotelName, o.DurationOfStay, o.StateOfStay, o.ChildrenPresence " +
      "FROM hotelNames c INNER JOIN durationOfStay o ON (c.HotelId = o.hotel_id.long)")

    resultTable.printSchema()

    val outputDf = resultTable.selectExpr("CAST(c.HotelName AS STRING) AS key",
      "CAST(CONCAT(o.DurationOfStay, ' ', o.StateOfStay, ' ', o.ChildrenPresence) AS STRING) AS value")
      .as[(String, String)]
    val topic = "KafkaTest"
    val broker = "localhost:9092"
    val writer = new KafkaSink(topic, broker)
    val query = outputDf
      .writeStream
      .foreach(writer)
      .outputMode("append")
      .start()
      .awaitTermination()

  }
}
